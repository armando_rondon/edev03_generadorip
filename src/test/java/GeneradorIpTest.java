

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GeneradorIpTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testGenerarNumero() {
		Clase_GeneradorIp ip = new Clase_GeneradorIp();
		String actual;
		String [] partes;
		for (int i = 0; i < 100; i++) {
			actual=	ip.generarIp();
			partes=	actual.split("\\.");
				for (int j = 0; j < partes.length; j++) {
				int tmp= Integer.parseInt(partes[j]);
				assertTrue(tmp>0 && tmp<254);
				}
		}
	}
	@Test
	void testGeneradorIp(){
		Clase_GeneradorIp ip = new Clase_GeneradorIp();
		String actual;
		String [] partes;
		actual=ip.generarIp();
		partes=actual.split("\\.");
		int numCero =Integer.parseInt(partes[0]);
		int numUltimo= Integer.parseInt(partes[partes.length-1]);
		assertTrue(numCero!=0 && numUltimo!=0);
	}


}
