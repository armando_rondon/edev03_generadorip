

import java.util.Random;

public class Clase_GeneradorIp {
	private int ipgenerador;
	/**
	 * 
	 * @param min
	 * @param max
	 * @return
	 */
	
	public int generaerNumero(int min, int max) {
		int numAlea;
		Random rnd = new Random();
		if(max > 255 || min < 0) {
			return -1;
		}
		numAlea=rnd.nextInt(max-min)+min;
		return numAlea;
	}
	
	public String generarIp() {
		String ip="";
		for (int i = 0; i < 4; i++) {
			ip+=this.generaerNumero(0, 255)+".";
		}
		return ip;
	}
	
}
